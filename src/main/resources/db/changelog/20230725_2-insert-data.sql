--liquibase formatted sql
--changeset microservice_class:20230725_2

INSERT INTO SAMPLE_TABLE
(id, product, updated_at)
VALUES ('6364bf5d-fa89-4ab3-b274-1da8cdb785b3', 'product-1', '2023-07-25 11:34:53.629'),
       ('66620d36-6a37-4ce3-9db0-783ee3fd11b4', 'product-2', '2023-07-25 11:35:53.629');

INSERT INTO TBL_CUSTOMERS
(ID ,FULL_NAME,DOB,ADDRESS,CUSTOMER_ID,PHONE,EMAIL,SALARY,JOB,UPDATED_AT)
VALUES ('6364bf5d-fa89-4ab3-b274-1da8cdb785b1', 'nguyen van a','01/01/2000','1 ha noi', 1, '0123','abc@abc.com',1000,'ky su','2023-07-25 11:34:53.629'),
       ('66620d36-6a37-4ce3-9db0-783ee3fd11b1', 'tran van b', '12/12/2000','2 ha noi', 2, '0123456','abc1@abc.com',2000,'cong nhan', '2023-07-25 11:35:53.629');