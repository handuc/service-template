package com.vietcombank.service.service;

import com.vietcombank.service.entity.CustomerEntity;
import com.vietcombank.service.repository.CustomerEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

@Slf4j
@org.springframework.stereotype.Service
public class CustomerService {
    @Autowired
    private CustomerEntityRepository customerEntityRepository;

    public List<CustomerEntity> retrieveAll() {
        return customerEntityRepository.findAll();
    }

    public CustomerEntity getDetail(UUID uuid) {
        return customerEntityRepository.findById(uuid).get();
    }

    public CustomerEntity getDetailByCif(int cif) {
        return customerEntityRepository.findBycustomerid(cif);
    }
    public void delete(UUID uuid) {
        customerEntityRepository.deleteById(uuid);
        return;
    }

    public void update(CustomerEntity customer) {
        customerEntityRepository.save(customer);
        return;
    }

    public void saveOrUpdate(CustomerEntity customer) {
        customerEntityRepository.save(customer);
        return;
    }
}
