package com.vietcombank.service.controller;

import com.vietcombank.service.entity.CustomerEntity;
import com.vietcombank.service.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/restapi")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping(value = "/customer/getall", produces = "application/json")
    @Operation(summary = "Retrieve All Customers")
    public ResponseEntity<?> retrieveCustomers() {
        log.info("retrieve all customers");
        return ResponseEntity.ok(customerService.retrieveAll());
    }

    @GetMapping(value = "/customer/get/{id}", produces = "application/json")
    @Operation(summary = "Get Details of a Customer")
    public ResponseEntity<?> getDetail(@PathVariable("id") UUID uuid) {
        return ResponseEntity.ok(customerService.getDetail(uuid));
    }
    @GetMapping(value = "/customer/getbycif/{cif}", produces = "application/json")
    @Operation(summary = "Get Details of a Customer")
    public ResponseEntity<?> getDetail(@PathVariable("cif") int cif) {
        return ResponseEntity.ok(customerService.getDetailByCif(cif));
    }

    @DeleteMapping(value = "/customer/delete/{id}")
    @Operation(summary = "Delete a Customer")
    public void deleteCustomer(@PathVariable("id") UUID uuid) {
        customerService.delete(uuid);
    }

    @PostMapping(value = "/customer")
    @Operation(summary = "Create a Customer")
    public ResponseEntity<?> saveCustomer(@RequestBody CustomerEntity customer) {
        customerService.saveOrUpdate(customer);
        return ResponseEntity.ok(customer.getId());
    }

    @PutMapping(value = "/customer")
    @Operation(summary = "Update details of a Customer")
    public ResponseEntity<?> updateCustomer(@RequestBody CustomerEntity customer) {
        customerService.saveOrUpdate(customer);
        return ResponseEntity.ok(customer);
    }
}