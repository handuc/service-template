package com.vietcombank.service.repository;

import com.vietcombank.service.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CustomerEntityRepository extends JpaRepository<CustomerEntity, UUID> {
    public CustomerEntity findBycustomerid(int cif);
}