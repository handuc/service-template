--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE SAMPLE_TABLE
(
    ID         uuid PRIMARY KEY default uuid_generate_v4(),
    PRODUCT    varchar(255),
    UPDATED_AT timestamp
);


CREATE TABLE TBL_CUSTOMERS
(
    ID         uuid PRIMARY KEY default uuid_generate_v4(),
    FULL_NAME    varchar(255),
    DOB    varchar(20),
    ADDRESS    varchar(255),
    CUSTOMER_ID  int,
    PHONE    varchar(20),
    EMAIL    varchar(100),
    SALARY    numeric(19,2),
    JOB    varchar(255),
    UPDATED_AT timestamp
);